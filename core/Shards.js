class Shards {
    constructor(prefix, path) {
        this.doSave = true;
        this.prefix = prefix;
        this.path = path;
        this.data = [];
    }

    add(shard, index) {
        /* Auto Index */
        if (!(index || false)) {
            index = this.getLength();
        }
        /* Placeholder */
        if (this.data[index] === undefined) {
            this.data[index] = [];
        }
        /* New shard */
        this.data[index].push(shard);
    }

    map(callback) {
        return this.data.map((cell, index) => {
            return callback(cell[0], index);
        });
    }

    reset() {
        this.data = [];
    }

    /*
     *	GETTERS
     */

    get(index) {
        if (this.data[index]) {
            return this.data[index];
        }
        return false;
    }

    getLength() {
        return this.data.length;
    }

    getLast(all = false) {
        if (this.data.length) {
            const cell = this.data.slice(-1)[0];
            return all ? cell : cell[0];
        }
        return false;
    }

    getData() {
        return this.data.map(cell => cell[0]);
    }

    /*
     *	SETTERS
     */

    setDontSave() {
        this.doSave = false;
    }

    setDoSave() {
        this.doSave = true;
    }
}

module.exports = Shards;
