const Shards = require('./Shards');
const sha256 = require('sha256');
const EC = require('elliptic').ec;
const EC_PROFILE = () => {
    return new EC('ed25519');
};
const path = require('path');
// const uuidv4 = require('uuid/v4');

class Licoin {
    constructor(blocks, deals) {
        this.name = 'Licoin';
        this.version = '1.0.0';
        /* Blocks */
        if (blocks instanceof Shards) {
            this.blocks = blocks;
        } else {
            this.blocks = new Shards('blocks', path.resolve('.'));
            this.setDontSave(true);
        }
        this.hashBlocks = this.blocks.getData().map(block => {
            return block.hash;
        });
        /* Deals */
        if (blocks instanceof Shards) {
            this.deals = deals;
        } else {
            this.deals = new Shards('deals', path.resolve('.'));
            this.setDontSave(true);
        }
        /* Add the genesis block */
        if (!this.blocks.getLength()) {
            const genesis = this.genesisBlock();
            this.blocks.add(genesis, 0);
            this.hashBlocks.push(genesis.hash);
        }
    }

    genesisBlock() {
        var block = {
            index: 0,
            prevHash: '0',
            timestamp: new Date().getTime(),
            data: 'LICOIN_GENESIS',
            version: this.version
        };
        block.hash = this.getHashBlock(block);
        return block;
    }

    /*
     *	BLOCKCHAIN
     */

    /* Create a simple block */
    createBlock(data) {
        const lastBlock = this.getLastBlock();
        /* Create a new block */
        var block = {
            index: lastBlock.index + 1,
            prevHash: lastBlock.hash,
            timestamp: new Date().getTime(),
            version: this.version,
            data
        };
        block.hash = this.getHashBlock(block);
        return block;
    }

    /* Check the block */
    checkBlock(block, lastBlock) {
        var check = true;
        /* Check the version */
        check &= block.version == this.version;
        /* Check the index */
        check &= block.index == lastBlock.index + 1;
        /* Check the previous hash */
        check &= block.prevHash == lastBlock.hash;
        /* Check the hash */
        check &= block.hash == this.getHashBlock(block);
        return check;
    }

    /* Add a block to the chain */
    addBlock(block) {
        if (this.checkBlock(block, this.getLastBlock())) {
            this.blocks.add(block, block.index);
            this.hashBlocks.push(block.hash);
            return block;
        }
        return false;
    }

    /* Build a block with the deals */
    buildBlock() {
        if (this.deals.getLength()) {
            const block = this.createBlock(this.deals.getData());
            this.deals.reset();
            return this.addBlock(block);
        }
        return false;
    }

    getBlock(n) {
        if (String(n).length == sha256('0').length) {
            const index = this.hashBlocks.indexOf(n);
            return index >= 0 ? this.blocks.getData()[index] : false;
        } else {
            return this.blocks.getData()[Number(n) >> 0] || false;
        }
    }

    getBlocks() {
        return this.blocks.getData();
    }

    getLastBlock() {
        return this.blocks.getLast();
    }

    getNbBlocks() {
        return this.blocks.getLength();
    }

    getHashBlock({ index, prevHash, timestamp, data, nounce }) {
        return sha256(
            JSON.stringify({ index, prevHash, timestamp, data, nounce })
        );
    }

    /*
     *	DEALS
     */

    createDeal({ to, from, data, keys }) {
        var deal = {
            from,
            to,
            data,
            timestamp: new Date().getTime()
        };
        deal.hash = this.getHashDeal(deal);
        /* Create a certificate */
        const ec = EC_PROFILE();
        const bunch = ec.keyFromPrivate(keys.priv, 'hex');
        const sign = bunch.sign(deal.hash).toDER();
        deal.sign = Buffer.from(sign).toString('hex');
        return deal;
    }

    checkDeal(deal, pubKey = '') {
        var check = true;
        /* Check the time */
        check &= deal.timestamp <= new Date().getTime();
        /* Check the hash */
        check &= deal.hash == this.getHashDeal(deal);
        /* Advance check */
        if (pubKey || false) {
            /* Check the address */
            check &= this.checkAddress(deal.from, pubKey);
            /* Check the certificate */
            const ec = EC_PROFILE();
            const bunch = ec.keyFromPublic(pubKey, 'hex');
            check &= bunch.verify(deal.hash, [
                ...Buffer.from(deal.sign, 'hex')
            ]);
        }
        return check;
    }

    addDeal(deal, pubKey) {
        if (this.checkDeal(deal, pubKey)) {
            this.deals.add(deal);
            return deal;
        }
        return false;
    }

    sendMsg(wallet, to, message) {
        const { priv, pub, address } = wallet;
        return this.addDeal(
            this.createDeal({
                to,
                from: address,
                data: { message },
                keys: { priv, pub }
            }),
            pub
        );
    }

    sendEmail(wallet, { to, author, subject, message }) {
        const { priv, pub, address } = wallet;
        return this.addDeal(
            this.createDeal({
                to,
                from: address,
                data: {
                    email: { author, subject, message }
                },
                keys: { priv, pub }
            }),
            pub
        );
    }

    publish(wallet) {
        const { priv, pub, address } = wallet;
        return this.addDeal(
            this.createDeal({
                to: address,
                from: address,
                data: { _key: pub },
                keys: { priv, pub }
            }),
            pub
        );
    }

    askToPublish(wallet, to) {
        const { priv, pub, address } = wallet;
        return this.addDeal(
            this.createDeal({
                to,
                from: address,
                data: { _ask: 'key' },
                keys: { priv, pub }
            }),
            pub
        );
    }

    getHashDeal({ from, to, data, timestamp }) {
        return sha256(JSON.stringify({ from, to, data, timestamp }));
    }

    /*
     *	ACCOUNTS
     */

    createWallet(seed = '') {
        const ec = EC_PROFILE();
        const bunch = ec.genKeyPair(seed ? { entropy: seed } : {});
        return {
            priv: bunch.getPrivate('hex'),
            pub: bunch.getPublic('hex'),
            address: this.createAddress(bunch.getPublic('hex'))
        };
    }

    createAddress(pubKey) {
        return sha256(pubKey).substr(0, 32);
    }

    checkAddress(address, pubKey) {
        return address == this.createAddress(pubKey);
    }

    _getDeals(address, search = 'to') {
        return this.blocks
            .map((block, index) => {
                if (!index) {
                    return [];
                }
                return block.data.map(deal => {
                    /* Search Mode: to, from, key */
                    if (search == 'to' || search == 'from') {
                        return deal[search] == address ? deal : false;
                    } else if (search == 'key') {
                        return deal.to == address &&
                            deal.from == address &&
                            deal.data._key !== undefined
                            ? deal
                            : false;
                    } else {
                        return false;
                    }
                });
            })
            .reduce((pack, deal) => {
                return [...pack, ...deal];
            }, [])
            .filter(deal => {
                return Boolean(deal);
            });
    }

    getReceiveDeals(address) {
        return this._getDeals(address, 'to');
    }

    getSentDeals(address) {
        return this._getDeals(address, 'from');
    }

    getPublicKeyFromAddress(address) {
        const blocks = this._getDeals(address, 'key');
        if (blocks.length) {
            const lastBlock = blocks[blocks.length - 1];
            if (this.checkAddress(address, lastBlock.data._key)) {
                return lastBlock.data._key;
            }
        }
        return false;
    }

    getPKA(address) {
        return this.getPublicKeyFromAddress(address);
    }
}

module.exports = Licoin;
