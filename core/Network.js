const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');

class Network {
    constructor(port = 666, ssl = false, limit = '20mb') {
        /* Settings */
        this.ssl = Boolean(ssl);
        this.port = Number(port);
        this.headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',
            'Access-Control-Allow-Headers':
                'Content-Type, Authorization, Content-Length, X-Requested-With',
            'Cache-Control': 'private, no-cache, no-store, must-revalidate',
            pragma: 'no-cache',
            Server: 'Licoin'
        };
        /* Data */
        this.flow = null;
        this.online = false;
        this.realmlist = [];
        /* Define a server */
        this.handle = express();
        this.handle.use(
            bodyParser.urlencoded({
                extended: true,
                limit
            })
        );
    }

    /* Connect with the Licoin instance */
    connect(flow) {
        if (flow && flow.constructor.name == 'Licoin') {
            this.flow = flow;
        }
        return this;
    }

    /* Start the server */
    start() {
        if (!this.online) {
            if (this.ssl) {
                console.log('SSL is not supported.');
                return false;
            } else {
                http.Server(this.handle).listen(this.port, '0.0.0.0', () => {
                    this.online = true;
                    console.log(
                        'The server is started on port ' + this.port + '.'
                    );
                });
            }
            /* Start the API */
            this.api();
        } else {
            console.log('Skip start. The server is also started.');
        }
        return this;
    }

    /* Main API */
    api() {
        /* Check the connection with Licoin */
        const checkFlow = reject => {
            if (this.flow !== null) {
                return true;
            }
            reject({
                status: 500,
                error:
                    'Internal Server Error: the server has not instance of Licoin.'
            });
            return false;
        };
        /* Ping-Pong */
        this.on('/ping', resolve => {
            resolve('pong');
        });
        /* Hello */
        this.on(/^\/?(\/hello)?$/, (resolve, reject) => {
            if (checkFlow(reject)) {
                resolve({
                    name: this.flow.name,
                    version: this.flow.version
                });
            }
        });
        /* Information about the blocks */
        this.on(/^\/blocks(\/([0-9a-f]+))?$/i, (resolve, reject, options) => {
            if (checkFlow(reject)) {
                const { params } = options;
                if (params[1] || false) {
                    const block = this.flow.getBlock(params[1]);
                    if (!block) {
                        reject({
                            status: 400,
                            error:
                                'Not Found: the block asked you is not found.'
                        });
                    }
                    resolve(block);
                } else {
                    resolve({
                        count: this.flow.getNbBlocks(),
                        genesis: this.flow.getBlock(0),
                        last: this.flow.getLastBlock()
                    });
                }
            }
        });
        /* Show the realmlist */
        this.on('/realmlist', resolve => {
            resolve(this.getReamList());
        });
        /* 404 */
        this.on('*', (resolve, reject) => {
            reject({
                status: 404,
                error: 'Not Found: please to Read The Fucking Manual !'
            });
        });
    }

    /* Listener */
    on(action, callback) {
        const self = this;
        self.handle.get(action, (req, res) => {
            /* Header */
            Object.entries(this.headers).map(([key, value]) => {
                res.header(key, value);
            });
            /* Define a simple promise */
            new Promise((resolve, reject) => {
                callback(resolve, reject, req);
            })
                .then(response => {
                    res.header('Status', 200);
                    res.writeHead(200, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.write(
                        JSON.stringify({
                            success: true,
                            data: response
                        })
                    );
                    res.end();
                })
                .catch(({ status, error }) => {
                    res.header('Status', status || 400);
                    res.writeHead(status || 400, {
                        'Content-Type': 'application/json; charset=utf-8'
                    });
                    res.write(
                        JSON.stringify({
                            success: false,
                            error: status || 400,
                            data: error
                        })
                    );
                    res.end();
                });
        });
    }

    /*
     *	SETTERS
     */

    addReamList(arg) {
        if (arg.constructor.name == 'String') {
            if (arg.indexOf('\n') < 0) {
                const regex = new RegExp(
                    '^(http)?(s|ssl|):?/{0,2}(([0-9]{1,3}\\.){3}' +
                        '[0-9]{1,3}|([^.]+\\.)?([^.]+\\.)?[^/:]+):?([0-9]+)?/?$'
                );
                /* Analyze */
                const search = arg.match(regex);
                if (search !== null) {
                    /* Extract the parameters */
                    const realm = [
                        ((search[2] || '').indexOf('s') == 0) >> 0,
                        search[3],
                        search[5] >> 0 || 80
                    ].join(':');
                    /* Add to realmlist */
                    if (this.realmlist.indexOf(realm) < 0) {
                        this.realmlist.push(realm);
                    }
                }
            } else {
                this.addReamList(arg.split('\n'));
            }
        } else if (arg.constructor.name == 'Array') {
            arg.map(item => {
                this.addReamList(item);
                return item;
            });
        }
        return this;
    }

    /*
     *	GETTERS
     */

    getReamList() {
        return this.realmlist;
    }
}

module.exports = Network;
