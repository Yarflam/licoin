class Account {
    constructor(chain, seed = '') {
        this.current = 0;
        this.chain = chain;
        this.wallets = [chain.createWallet(seed)];
    }

    /* GETTERS */

    getWallet() {
        return this.wallets[this.current];
    }

    _getDeals(method) {
        if (method == 'getReceiveDeals' || method == 'getSentDeals') {
            return this.wallets
                .map(wallet => {
                    return this.chain[method](wallet.address);
                })
                .reduce((accum, shard) => {
                    return accum.concat(shard);
                }, []);
        }
        return [];
    }

    getReceiveDeals() {
        return this._getDeals('getReceiveDeals');
    }

    getSentDeals() {
        return this._getDeals('getSentDeals');
    }

    /* SETTERS */

    setCurrent(index) {
        if (this.wallets[index] !== undefined) {
            this.current = index;
            return true;
        }
        return false;
    }
}

module.exports = Account;
