const sha256 = require('sha256');

const randInt = (a, b) => {
    return Math.floor(Math.random() * (b - a)) + a;
};

const mstr = (n, str) => {
    return new Array(n + 1).join(str);
};

/*
 *	Proof Of Work
 */
const PoW = (nbZeros, iMax) => {
    const sample = mstr(nbZeros, '0');
    const initial = sha256(String(randInt(0, 2000)));
    var proof = initial;
    /* Iterates */
    let i = 0;
    while (i < iMax && proof.substr(0, nbZeros) != sample) {
        proof = sha256(proof);
        if (proof == initial) {
            break;
        }
        i++;
    }
    /* Return the response */
    if (i < iMax && proof != initial) {
        return (
            'Found: ' +
            proof +
            ' (with ' +
            nbZeros +
            ' zeros, ' +
            i +
            ' iterations)'
        );
    } else if (proof == initial) {
        return 'Loop detected';
    } else {
        return 'Not found';
    }
};

(() => {
    const iMax = 100000000;
    for (let i = 1; i < 7; i++) {
        console.log(i, PoW(i, iMax));
    }
})();

/*
 *	1 'Found: 072320acf9cc97e5e8a40ab0372fe99139c225e0ba2e6797584c1f4e1bf7447e (with 1 zeros, 2 iterations)'
 *	2 'Found: 001fb509d57bcf470d642dde7266ba2c5254e11d65c1d3ce1c1be0da166e6bb8 (with 2 zeros, 329 iterations)'
 *	3 'Found: 0008adf5ba61f2f5860ea102890ee157efa794ac7dbd4ac0546cac86a3363fab (with 3 zeros, 1649 iterations)'
 *	4 'Found: 0000d13a1df105d39c093d58ca46e5dc19976db3e2baeaf4185bfbc4010a03ff (with 4 zeros, 79356 iterations)'
 *	5 'Found: 00000077b2ea82fd7e06f6ddb4da2d52d3aec765a6992b74412f3b212fa1712a (with 5 zeros, 962676 iterations)'
 *	6 'Found: 0000001ca3b951fe148eb3e82355c463a0b57d4284ecb819acf696e5d7f3993d (with 6 zeros, 13490849 iterations)'
 */
