# ![logo](public/images/logo32x32.png) Licoin

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)
![version](https://img.shields.io/badge/app-vDev-blue.svg)

A small project to study the blockchain.

*Read this in other languages: [English](README.md), [Français](README.fr.md)*.

## Prerequisites

You have to install Node.js on your computer (Windows / Linux / Mac / ARM systems).

[Download here](https://nodejs.org/en/download/)

## Install

You can clone the project:

> $> git clone https://gitlab.com/Yarflam/licoin
>
> $> npm install

Or install as a node module (recommended):

> $> npm install https://gitlab.com/Yarflam/licoin

## Abstract

Methods:
- Hash: sha256
- Signature: EdDSA (Curve25519)

Classes:
- Account: manage the wallets.
- Licoin: manage the Licon blockchain.
- Shards: manage the blockchain's data.

### Proof Of Knowledge

Before to explain the Proof of Knowledge, we have to describe the others consensus methods.

- **Proof of Work**: with Bitcoin, the minors must find a hash beginning with zeros.
The proof is a random string and his difficulty depends on number of zeros asked.
Variants exist to use others calculations, look for the pi's decimals for example.
- **Proof of Stack**: you are choose by a lottery. If you are rich on the network
or that you do many transactions, you are more lucky !
- **Proof of Capacity**: you have to allocate memory on your computer and prove
it with a measure ; you can't cheat because the challenge require a large memory
each time you participate.
- **Proof of Authority**: it's not really interesting, you give the rights
to create of the blocks to an authority group (they can decide everything, create
or not).

=== Draft ===

The Proof of Knowledge wants to give the create privilege to the user that can
take capture the biggest number of transactions because he knows a large network.

=== end of Draft (because I have many problem not solved) ===

## Initialize

Import the module:

```javascript
const {Account, Shards, Licoin} = require('licoin');
```

Create the instance of Licoin:

```javascript
const app = new Licoin(
    new Shards('blocks', path.resolve( 'data' )),
    new Shards('deals', path.resolve( 'data' ))
);
```

## Account & Wallets

Create an account:

```javascript
const account = new Account(app);
```

Get the wallet selected (first by default):

```javascript
const wallet = account.getWallet();
```

Show your public address:

```javascript
console.log(wallet.address);
```

Share your public key on the blockchain:

```javascript
app.publish(wallet);
// return a deal
```

Wallet Structure:

```json
{
    "priv": "0d5ae...",
    "pub": "043e1...",
    "address": "f3af2..."
}
```

## Deals

Send a simple message:

```javascript
app.sendMsg(wallet, recipient_address, 'My message');
// return a deal
```

Send a simple email:

```javascript
app.sendEmail(wallet, {
    to: recipient_address,
    author: 'Licoin Guest',
    subject: 'Subject',
    message: 'My Message'
});
// return a deal
```

Check a deal with the signature:

```javascript
console.log(app.checkDeal(deal, wallet.pub));
// return true or false
```

Deal Structure:

```json
{
    "from": "f3af2...",
    "to": "d7e09...",
    "data": { "message": "My Message" },
    "timestamp": "1549194707477",
    "hash": "f61a8...",
    "sign": "30440..."
}
```

## Block

Currently, the Proof of Knowledge is not implemented.

Create a block with the deals:

```javascript
app.buildBlock();
```

Block Structure:

```json
{
    "index": 1,
    "prevHash": "bf666...",
    "timestamp": "1549194707477",
    "version": "1.0.0",
    "data": [ "Object<Deal>:0", "Object<Deal>:1" ],
    "hash": "d8a0e..."
}
```

## Read the blocks

Read my receive deals (to: <wallet.address>):

```javascript
account.getReceiveDeals()
    .map(deal => {
        console.log(deal);
    })
```

Read my sent deals (from: <wallet.address>):

```javascript
account.getSentDeals()
    .map(deal => {
        console.log(deal);
    })
```

Get a public key on the blockchain:

```javascript
console.log(app.getPKA(address));
```

## Acknowledgments

You can run the example (to discover features):

> $> node node_modules/licoin/example.js

Currently, many features are not implemented:

- Shard class don't save the blockchain on the filesystem.
- Shard and Licon classes don't manage the fork chain and conflict.
- You can add a block without resolve a Proof of Knowledge.

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](LICENSE.md) file for details.

## Documents ressources

[Medium.com - good article](https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54)
