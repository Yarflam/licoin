const { Account, Licoin, Network, Shards } = require('./core');
const path = require('path');
const fs = require('fs');

/* Licoin App */
const app = new Licoin(
    new Shards('blocks', path.resolve('data')),
    new Shards('deals', path.resolve('data'))
);

/* Publish Licoin on the web (API JSON) */
new Network()
    .connect(app)
    .addReamList(fs.readFileSync(path.resolve('realmlist.txt')).toString())
    .start();

/* Create an account */
const account = new Account(app);
const wallet = account.getWallet();

/* Create blocks */
app.publish(wallet);
app.buildBlock();
app.sendMsg(wallet, '172320dcf9cc97', 'A');
app.buildBlock();
app.sendMsg(wallet, '172320dcf9cc98', 'B');
app.buildBlock();
app.sendMsg(wallet, '172320dcf9cc99', 'C');
app.buildBlock();
app.sendMsg(wallet, '172320dcf9ccA0', 'D');
app.buildBlock();
