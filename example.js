const { Account, Licoin, Shards } = require('./core');
const path = require('path');

/* Licoin App */
const app = new Licoin(
    new Shards('blocks', path.resolve('data')),
    new Shards('deals', path.resolve('data'))
);

/* Create account */
var accounts = {
    alice: new Account(app),
    bob: new Account(app)
};

/* Get the wallets */
var wallets = {
    alice: accounts.alice.getWallet(),
    bob: accounts.bob.getWallet()
};

/* Alice -> Bob : send a message */
app.sendMsg(
    wallets.alice,
    wallets.bob.address,
    'Hello Bob ! How are you today ?'
);
app.sendEmail(wallets.alice, {
    to: wallets.bob.address,
    author: 'Alice',
    subject: 'Meeting',
    message: 'I need to meet you tomorrow at 6 PM.'
});
app.publish(wallets.alice);
app.buildBlock();

/* Bob -> Alice : send a message */
app.sendMsg(wallets.bob, wallets.alice.address, 'Hey :)');
app.buildBlock();

/* Show the message from address */
const showMsgService = account => {
    account.getReceiveDeals().map(deal => {
        /* It's a message or email */
        if (deal.data.message || deal.data.email || false) {
            /* Check the signature */
            const pka = app.getPKA(deal.from);
            const check = pka && app.checkDeal(deal, pka);
            /* Show the header */
            if (!check) {
                console.log('[[ WARNING: NOT SECURE ADDRESS ]]');
            }
            console.log('Date: ' + new Date(deal.timestamp));
            /* Show the content */
            if (deal.data.message || false) {
                console.log('From: <' + deal.from + '>');
                console.log('Message: ' + deal.data.message);
            } else {
                console.log(
                    'From: ' + deal.data.email.author + ' <' + deal.from + '>'
                );
                console.log('Subject: ' + deal.data.email.subject);
                console.log(deal.data.email.message);
            }
            console.log('.');
        }
        return deal;
    });
};

showMsgService(accounts.alice);
showMsgService(accounts.bob);
console.log(app.getBlocks());
