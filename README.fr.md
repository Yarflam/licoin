# ![logo](public/images/logo32x32.png) Licoin

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)
![version](https://img.shields.io/badge/app-vDev-blue.svg)

Un petit projet maison pour apprendre à développer une blockchain.

*Pour lire cette page dans une autre langue : [English](README.md), [Français](README.fr.md)*.

## Pré-requis

Il est nécessaire d'installer Node.js (Windows / Linux / Mac / systèmes ARM).

[A télécharger ici](https://nodejs.org/en/download/)

## Installation

On peut cloner le projet :

> $> git clone https://gitlab.com/Yarflam/licoin
>
> $> npm install

Ou mieux, l'installer comme un module (recommandée) :

> $> npm install https://gitlab.com/Yarflam/licoin

## Résumé

Méthodes utilisées :
- Hash : sha256
- Signature : EdDSA (Curve25519)

Classes :
- Compte : gérer les portemonnaies.
- Licoin : gérer la blockchain Licoin.
- Shards : gérer les données de la blockchain.

### Preuve de Connaissance

Avant d'expliquer la Preuve de Connaissance, nous avons besoin d'analyser les
autres méthodes de consensus.

- **Preuve de Travail** : avec Bitcoin, on voit que les mineurs doivent trouver
un hash qui commencent avec un certain nombres de zéros. La preuve est une chaîne
aléatoire et sa difficulté dépend du nombre de zéros demandé. Des variantes
existent en utilisant d'autres calculs, notamment trouver les décimales de pi.
- **Preuve d'Enjeu** : vous êtes sélectionné par tirage au sort. Si vous êtes
riche sur le réseau ou que vous effectuez beaucoup de transactions, vos chances
augmentent !
- **Preuve de Capacité** : vous devez allouer de la mémoire sur votre ordinateur
avant d'effectuer une mesure pour prouver votre participation. La triche est
évitée car la preuve nécessite beaucoup de ressources chaque fois que vous
participez au consensus.
- **Preuve d'Autorité** : ce n'est pas très intéressant, vous accordez les droits
de créer des blocs à un groupe autoritaire (ils peuvent tout décider, accepter
    ou refuser la création).

=== Brouillon ===

La Preuve de Connaissance souhaite donner le privilège de création à
l'utilisateur capable de capturer le maximum de transactions car cela pouvrerait
qu'il connaît un large réseau.

=== fin du Brouillons (parce que j'ai encore beaucoup de problèmes à résoudre) ===

## Initialisation

Importer le module :

```javascript
const {Account, Shards, Licoin} = require('licoin');
```

Créer l'instance Licoin :

```javascript
const app = new Licoin(
    new Shards('blocks', path.resolve( 'data' )),
    new Shards('deals', path.resolve( 'data' ))
);
```

## Compte & Portemonnaies

Créer un compte :

```javascript
const account = new Account(app);
```

Récupérer le portemonnaie sélectionné (par défaut le premier) :

```javascript
const wallet = account.getWallet();
```

Afficher votre adresse publique :

```javascript
console.log(wallet.address);
```

Publier votre clé publique sur la blockchain :

```javascript
app.publish(wallet);
// retourne une transaction
```

Structure d'un portemonnaie (Wallet) :

```json
{
    "priv": "0d5ae...",
    "pub": "043e1...",
    "address": "f3af2..."
}
```

## Transactions

Envoyer un message :

```javascript
app.sendMsg(wallet, recipient_address, 'My message');
// retourne une transaction
```

Envoyer un email :

```javascript
app.sendEmail(wallet, {
    to: recipient_address,
    author: 'Licoin Guest',
    subject: 'Subject',
    message: 'My Message'
});
// retourne une transaction
```

Vérifier une transaction à partir de la signature :

```javascript
console.log(app.checkDeal(deal, wallet.pub));
// retourne vrai (true) ou faux (false)
```

Structure d'une transaction (Deal) :

```json
{
    "from": "f3af2...",
    "to": "d7e09...",
    "data": { "message": "My Message" },
    "timestamp": "1549194707477",
    "hash": "f61a8...",
    "sign": "30440..."
}
```

## Bloc

Actuellement, la Preuve de Connaissance n'est pas implémentée.

Créer un bloc à partir des transactions :

```javascript
app.buildBlock();
```

Structure d'un bloc (Block) :

```json
{
    "index": 1,
    "prevHash": "bf666...",
    "timestamp": "1549194707477",
    "version": "1.0.0",
    "data": [ "Object<Deal>:0", "Object<Deal>:1" ],
    "hash": "d8a0e..."
}
```

## Examiner les blocs

Examiner mes transactions reçues (from: <wallet.address>) :

```javascript
account.getReceiveDeals()
    .map(deal => {
        console.log(deal);
    })
```

Examiner mes transactions envoyées (from: <wallet.address>) :

```javascript
account.getSentDeals()
    .map(deal => {
        console.log(deal);
    })
```

Récupérer une clé publique sur la blockchain :

```javascript
console.log(app.getPKA(address));
```

# A savoir

Vous pouvez tester l'exemple (pour découvrir les fonctionnalités) :

> $> node node_modules/licoin/example.js

Actuellement, plusieurs fonctionnalités ne sont pas implémentées :

- La classe Shard n'enregistre pas la blockchain sur le système de fichiers.
- Les classes Shard et Licoin ne gèrent pas les chaînes conflictuelles.
- Vous pouvez ajouter un bloc sans résoudre une Preuve de Connaissance.

## Auteurs

- Yarflam - *travail initial*

## Licence

Le projet est sous licence Creative Commons (BY-NC-SA) - regardez le fichier
[LICENSE.md](LICENSE.md) pour en savoir plus.

## Ressources documentaires

[Medium.com - bon article de vulgarisation](https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54)
